from django.contrib import admin
from .models import (
    Product,
    ProductAbout,
    ProductDetail,
    ProductMedia,
    ProductQuestion,
    ProductReview,
    ProductReviewVoting,
    ProductTag,
    ProductTransaction,
    ProductVariantItem,
    ProductVariantTitle
)

# Register your models here.


admin.site.register(Product)
admin.site.register(ProductAbout)
admin.site.register(ProductDetail)
admin.site.register(ProductMedia)
admin.site.register(ProductQuestion)
admin.site.register(ProductReview)
admin.site.register(ProductReviewVoting)
admin.site.register(ProductTag)
admin.site.register(ProductTransaction)
admin.site.register(ProductVariantItem)
admin.site.register(ProductVariantTitle)
