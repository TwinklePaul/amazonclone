from django.shortcuts import render
from django.views.generic import (
    ListView,
    CreateView,
    UpdateView,
    DetailView,
    View,
    FormView
)
from .models import Product
from user_types.models import AppUser
from Categories.models import Category
from Subcategory.models import SubCategory
from Merchant.models import Merchant
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from .forms import ProductForm
import logging


# Create your views here.


class ProductCreateView_Admin(LoginRequiredMixin, FormView):
    form_class = ProductForm
    template_name = 'Products/admin_templates/edit.html'


def load_subcategories(request):
    selected_category = request.GET.get('category')
    cat = Category.objects.filter(title=selected_category)[0]

    subcategories = SubCategory.objects.filter(
        category=cat)
    return render(
        request,
        'Products/admin_templates/subcategory_deopdown.html',
        {'subcategories': subcategories})


class ProductListView_Admin(LoginRequiredMixin, View):

    def get(self, **kwargs):
        return render(self.request, 'Products/admin_templates/list.html')


# class MerchantUpdate_Admin(LoginRequiredMixin, UserPassesTestMixin, SuccessMessageMixin, UpdateView):
#     model = Merchant
#     success_message = "Merchant updated successfully!"
#     fields = [
#         "profile_pic",
#         "company_name",
#         "gst_Details"
#     ]
#     template_name = 'Merchant/admin_templates/edit.html'

#     def test_func(self):
#         merchant = self.get_object()

#         return True


# class MerchantDetailView_Admin(LoginRequiredMixin, DetailView):
#     model = Merchant
#     template_name = 'Merchant/admin_templates/details.html'
#     context_object_name = 'merchant'
