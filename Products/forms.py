from django import forms
from .models import (
    Product
)
from Categories.models import Category as Cat
from Subcategory.models import SubCategory as SubCat
from Merchant.models import Merchant


class ProductForm(forms.ModelForm):
    Category = forms.ModelChoiceField(
        queryset=Cat.objects.filter(is_active=True).values_list('title', flat=True))

    Subcategory = forms.ModelChoiceField(
        queryset=SubCat.objects.none())

    Merchant = forms.ModelChoiceField(queryset=Merchant.objects.filter(
        is_active=True))

    ordered_field_names = ['Category', 'SubCategory', 'Merchant']

    class Meta:
        model = Product
        fields = ('name', 'brand', 'description',
                  'long_description', 'max_price', 'discount_percent')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['Subcategory'].queryset = SubCat.objects.none()

        if 'Category' in self.data:
            try:
                category = self.data.get('Category')
                self.fields['Subcategory'].queryset = SubCat.objects.filter(
                    category=Cat.objects.filter(title=category[0])).values_list('title',  flat=True)
            except (ValueError, TypeError):
                pass

        # self.helper = FormHelper()
        # self.helper.layout = Layout(
        #     Fieldset(
        #         'Category', 'SubCategory', 'Merchant', self.fields
        #     ))
