from django.urls import path
from .views import (
    ProductListView_Admin,
    ProductCreateView_Admin
)
from . import views

urlpatterns = [
    path('', ProductListView_Admin.as_view(), name='product-list'),
    path('create/', ProductCreateView_Admin.as_view(), name='product-create'),
    path('ajax/load-subcategories/', views.load_subcategories,
         name='ajax_load-subcategories')
]
