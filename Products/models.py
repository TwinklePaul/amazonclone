from django.db import models
from Subcategory.models import SubCategory
from user_types.models import AppUser
from Customer.models import Customer
from Merchant.models import Merchant
from django.core.validators import MaxValueValidator, MinValueValidator


# Create your models here.


class Product(models.Model):
    id = models.AutoField(primary_key=True)
    subcategory = models.ForeignKey(SubCategory, on_delete=models.CASCADE)
    url_slug = models.CharField(max_length=120)

    name = models.CharField(max_length=255)
    brand = models.CharField(max_length=120)
    description = models.TextField()
    long_description = models.TextField()
    added_by_merchant = models.ForeignKey(
        Merchant, on_delete=models.CASCADE)

    max_price = models.FloatField()
    discount_percent = models.FloatField()
    discount_price = models.FloatField()

    # url_slug = models.CharField(max_length=50)

    created_at = models.DateTimeField(auto_now_add=True)
    is_active = models.BooleanField(default=True)

    class Meta:
        verbose_name_plural = 'Products'

    def __str__(self):
        return f'{self.name} - {self.brand} Brand'

    def save(self, *args, **kwargs):
        self.discount_price = self.max_price - \
            (self.max_price - self.discount_percent)/100
        return super().save(*args, **kwargs)


class ProductDetail(models.Model):
    id = models.AutoField(primary_key=True)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)

    feature = models.CharField(max_length=255)
    in_stock = models.BooleanField(default=True)
    feature_details = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return f'Details on {self.product.name}, {self.product.brand} Brand'


class ProductMedia(models.Model):
    id = models.AutoField(primary_key=True)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)

    MEDIA_TYPE_CHOICES = [
        (1, 'Image'),
        (2, 'Video'),
    ]
    media_type = models.PositiveSmallIntegerField(
        choices=MEDIA_TYPE_CHOICES, default=1)
    media_content = models.FileField(upload_to='product_media')

    created_at = models.DateTimeField(auto_now_add=True)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return f'{self.media_type} for {self.name} - {self.brand} Brand'


class ProductTransaction(models.Model):
    id = models.AutoField(primary_key=True)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)

    TRANSACTION_TYPE_CHOICES = [
        (1, 'Buy'),
        (2, 'Sell'),
    ]
    transaction_type = models.PositiveSmallIntegerField(
        choices=TRANSACTION_TYPE_CHOICES, default=1)
    transaction_count = models.IntegerField()
    transaction_description = models.TextField()

    created_at = models.DateTimeField(auto_now_add=True)


class ProductAbout(models.Model):
    id = models.AutoField(primary_key=True)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)

    # title = models.CharField(max_length=255)
    information = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return f'About {self.product.name}, {self.product.brand} Brand'


class ProductTag(models.Model):
    id = models.AutoField(primary_key=True)
    product_id = models.ForeignKey(Product, on_delete=models.CASCADE)

    tags = models.CharField(max_length=120)
    created_at = models.DateTimeField(auto_now_add=True)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return f'Tag: {self.tags} - {self.product.name}, {self.product.brand} Brand'


class ProductQuestion(models.Model):
    id = models.AutoField(primary_key=True)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    user = models.ForeignKey(AppUser, on_delete=models.CASCADE, null=True)

    questions = models.TextField()
    answers = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return f'{self.product.name}, {self.product.brand} Brand: QuestionId - {self.id}'


class ProductReview(models.Model):
    id = models.AutoField(primary_key=True)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    user = models.ForeignKey(Customer, on_delete=models.CASCADE)

    rating = models.FloatField(default=5.0,
                               validators=[MinValueValidator(1.0), MaxValueValidator(5.0)])
    review_image = models.ImageField(upload_to='review_image')
    title = models.CharField(max_length=100)
    review = models.TextField(default="")
    created_at = models.DateTimeField(auto_now_add=True)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return f'{self.product.name}, {self.product.brand} Brand: Review By - {self.user.name}'


class ProductReviewVoting(models.Model):
    id = models.AutoField(primary_key=True)
    product_review = models.ForeignKey(
        ProductReview, on_delete=models.CASCADE)
    user_id_voting = models.ForeignKey(Customer, on_delete=models.CASCADE)

    created_at = models.DateTimeField(auto_now_add=True)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return f'Review: {self.product_review.id},  Voted By - {self.user_id_voting.name}'


class ProductVariantTitle(models.Model):
    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=120)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f'Variant Title: {self.title}'


class ProductVariantItem(models.Model):
    id = models.AutoField(primary_key=True)
    category = models.ForeignKey(
        ProductVariantTitle, on_delete=models.CASCADE)
    product_id = models.ForeignKey(Product, on_delete=models.CASCADE)

    variant_type = models.CharField(max_length=80)

    def __str__(self):
        return f'{self.category.title}: {self.variant_type}'
