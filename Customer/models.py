from django.db import models
from django.urls import reverse
from user_types.models import AppUser

# Create your models here.


class Customer(models.Model):
    user = models.OneToOneField(
        AppUser, on_delete=models.CASCADE)

    profile_pic = models.ImageField(
        default='default.png', upload_to='customer_profile_pic')

    def __str__(self):
        return f'{self.user.user_name} - Customer'

    def get_absolute_url(self):
        return reverse("customer-list")
