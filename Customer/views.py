from django.shortcuts import render
from django.views.generic import (
    ListView,
    CreateView,
    UpdateView,
    DetailView
)
from .models import Customer
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin

# Create your views here.


class CustomerCreateView_Admin(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    model = Customer
    success_message = "Customer updated successfully!"
    fields = "__all__"
    template_name = 'Customer/admin_templates/edit.html'


class CustomerListView_Admin(LoginRequiredMixin, ListView):
    model = Customer
    template_name = 'Customer/admin_templates/list.html'
    context_object_name = 'customers'


# class CustomerUpdate_Admin(LoginRequiredMixin, UserPassesTestMixin, SuccessMessageMixin, UpdateView):
#     model = Customer
#     success_message = "Customer updated successfully!"
#     fields = "__all__"
#     template_name = 'Customer/admin_templates/edit.html'

#     def test_func(self):
#         customer = self.get_object()

#         return True
