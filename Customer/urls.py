from django.urls import path
from .views import (
    CustomerListView_Admin,
    CustomerCreateView_Admin,
    #     SubCategoryUpdate_Admin,
)

urlpatterns = [
    path('', CustomerListView_Admin.as_view(), name="customer-list"),
    path('createcustomer/', CustomerCreateView_Admin.as_view(),
         name="customer-create"),
    # path('updatesub/<slug:pk>', SubCategoryUpdate_Admin.as_view(),
    #      name="subcategory-update"),
]
