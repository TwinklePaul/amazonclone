from django.shortcuts import render
from django.views.generic import (
    ListView,
    CreateView,
    UpdateView,
    DeleteView
)
from .models import SubCategory
from Categories.models import Category
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin

# Create your views here.


class SubCategoryListView_Admin(LoginRequiredMixin, ListView):
    model = SubCategory
    template_name = 'Subcategory/admin_templates/list.html'
    context_object_name = 'subcategories'


class SubCategoryCreateView_Admin(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    model = SubCategory
    success_message = "Sub-category created successfully!"
    fields = "__all__"
    template_name = 'Subcategory/admin_templates/edit.html'


class SubCategoryUpdate_Admin(LoginRequiredMixin, UserPassesTestMixin, SuccessMessageMixin, UpdateView):
    model = SubCategory
    success_message = "Sub-category updated successfully!"
    fields = "__all__"
    template_name = 'Subcategory/admin_templates/edit.html'

    def test_func(self):
        subcategory = self.get_object()

        return True


class SubCategoryDelete_Admin(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    model = SubCategory
    template_name = 'Subcategory/admin_templates/delete.html'
    success_url = '/subcategory'

    def test_func(self):
        subcategory = self.get_object()
        if self.request.user.user_type == 1:
            return True
        return False


class CategoryDetails_Admin(LoginRequiredMixin, ListView):
    model = SubCategory

    template_name = 'Subcategory/admin_templates/list.html'
    context_object_name = 'subcategories'

    def get_queryset(self):
        return SubCategory.objects.filter(category=Category.objects.filter(url_slug=self.kwargs['slug'])[0])
