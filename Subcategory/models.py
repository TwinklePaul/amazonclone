from django.db import models
from Categories.models import Category
from django.urls import reverse

# Create your models here.


class SubCategory(models.Model):
    id = models.AutoField(primary_key=True)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)

    title = models.CharField(max_length=120)
    url_slug = models.CharField(max_length=50)
    thumbnail = models.ImageField(upload_to='subcategory_images')
    description = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    is_active = models.BooleanField(default=True)

    class Meta:
        verbose_name_plural = 'Subcategories'

    def __str__(self):
        return f'{self.title}'

    def get_absolute_url(self):
        return reverse("subcategory-list")
