from django.urls import path
from .views import (
    SubCategoryListView_Admin,
    SubCategoryCreateView_Admin,
    SubCategoryUpdate_Admin,
    SubCategoryDelete_Admin
)

urlpatterns = [

    path('', SubCategoryListView_Admin.as_view(), name="subcategory-list"),
    path('createsub/', SubCategoryCreateView_Admin.as_view(),
         name="subcategory-create"),
    path('updatesub/<slug:pk>', SubCategoryUpdate_Admin.as_view(),
         name="subcategory-update"),
    path('delete/<int:pk>/', SubCategoryDelete_Admin.as_view(),
         name='subcategory-delete'),
]
