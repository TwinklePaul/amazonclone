from django.shortcuts import render

# Create your views here.
from django.shortcuts import render
from django.views.generic import (
    ListView,
    CreateView,
    UpdateView,
    DetailView
)
from .models import Staff
from user_types.models import AppUser
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin

# Create your views here.


class StaffCreateView_Admin(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    model = Staff
    success_message = "Staff updated successfully!"
    fields = "__all__"
    template_name = 'Staff/admin_templates/edit.html'


class StaffListView_Admin(LoginRequiredMixin, ListView):
    model = Staff
    template_name = 'Staff/admin_templates/list.html'
    context_object_name = 'staffs'


class StaffUpdate_Admin(LoginRequiredMixin, UserPassesTestMixin, SuccessMessageMixin, UpdateView):
    model = Staff
    success_message = "Staff updated successfully!"
    fields = "__all__"
    template_name = 'Staff/admin_templates/edit.html'

    def test_func(self):
        staff = self.get_object()

        return True
