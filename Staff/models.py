from django.db import models
from user_types.models import AppUser
from django.urls import reverse

# Create your models here.


class Staff(models.Model):
    user = models.OneToOneField(
        AppUser, on_delete=models.CASCADE)

    profile_pic = models.ImageField(
        default='default.png', upload_to='staff_profile_pic')

    def __str__(self):
        return f'{self.user.user_name} - Staff'

    def get_absolute_url(self):
        return reverse('staff-list')
