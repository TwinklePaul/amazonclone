from django.urls import path
from .views import (
    StaffCreateView_Admin,
    StaffListView_Admin,
    StaffUpdate_Admin,
)

urlpatterns = [

    path('', StaffListView_Admin.as_view(), name="staff-list"),
    path('createstaff/', StaffCreateView_Admin.as_view(),
         name="staff-create"),
    # path('updatesub/<slug:pk>', SubCategoryUpdate_Admin.as_view(),
    #      name="subcategory-update"),
]
