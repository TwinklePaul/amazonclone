from django.contrib import admin
from django.db import models
from .models import (
    AppUser,
    Admin,
)
from django.contrib.auth.admin import UserAdmin
from django.forms import TextInput, Textarea


# class ItemAdmin(admin.ModelAdmin):
#     exclude = ('is_staff',
#                'is_superuser')


class UserAdminConfig(UserAdmin):
    model = AppUser

    readonly_fields = ('is_staff',
                       'is_superuser',
                       )

    search_fields = (
        'email',
        'user_name',
    )

    list_filter = (
        'user_name',
        'email',
        'user_type',
        'phone_number',
        'address',
        'is_active',
        'is_staff',
        'is_superuser'
    )

    ordering = ('-created_at',)

    list_display = (
        'user_name',
        'email',
        'user_type',
        'phone_number',
        'address',
        'is_active',
        'is_staff',
        'is_superuser'
    )

    fieldsets = (
        (None, {
            'fields': (
                'user_name',
                'email',
            )
        }),

        ('Permissions', {
            'fields': (
                'user_type',
                'is_active',
                'is_staff',
                'is_superuser'
            )
        }),

        ('Personal', {
            'fields': (
                'phone_number',
                'address',
            )
        }),
    )

    formfield_overrides = {
        models.TextField: {'widget': Textarea(attrs={'rows': 10, 'cols': 47})},
        models.IntegerField: {'widget': TextInput(attrs={'size': '12'})}

    }

    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'user_name', 'user_type', 'phone_number', 'address', 'password1', 'password2')}
         ),
    )


admin.site.register(AppUser, UserAdminConfig)

# Register your models here.
admin.site.register(Admin)
