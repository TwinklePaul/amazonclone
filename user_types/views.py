from django.http.response import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.shortcuts import redirect, render
from django.views.generic import (
    ListView,
    CreateView,
    UpdateView,
    DetailView
)
from .models import AppUser
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin

# Create your views here.


def adminLogin(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(
            request=request, user_name=username, password=password)

        if user is not None:
            login(request=request, user=user)
            return redirect('admin-home')
        else:
            messages.error(
                request, f"Invalid Login Credentials!")
            return redirect('admin-login')

    return render(request, 'user_types/admin_templates/sigin.html')


@login_required(login_url='admin-login')
def adminLogout(request):
    logout(request)
    messages.success(request, f"Logged out successfully!")
    return redirect('admin-login')


@login_required(login_url='admin-login')
def adminHome(request):
    return render(request, 'user_types/admin_templates/home.html')


class CreateUser_Admin(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    model = AppUser
    success_message = "User added successfully!"
    template_name = 'user_types/admin_templates/edituser.html'
    fields = ['user_type',
              'name',
              'user_name',
              'email',
              'phone_number',
              'address',
              'password'
              ]

    def form_valid(self, form):
        user = form.save()
        user.is_active = True
        user.set_password(form.cleaned_data['password'])
        user.save()

        return HttpResponseRedirect(reverse("admin-user-list"))


class UserListView_Admin(LoginRequiredMixin, ListView):
    model = AppUser
    template_name = 'user_types/admin_templates/listusers.html'
    context_object_name = 'users'


class UserDetailView_Admin(LoginRequiredMixin, DetailView):
    model = AppUser
    template_name = 'user_types/admin_templates/userdetails.html'
    context_object_name = 'user'
