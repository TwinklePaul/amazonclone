from django.db import models
from django.urls import reverse
from django.utils.translation import gettext_lazy as _

from django.contrib.auth.models import (
    AbstractBaseUser,
    BaseUserManager,
    PermissionsMixin
)


# Create your models here.


class CustomAccountManager(BaseUserManager):

    def create_user(self, user_type, user_name, email, phone_number, address, password, **other_fields):

        if not email:
            raise ValueError(_('You must provide an email address'))

        email = self.normalize_email(email)

        user = self.model(
            email=email,
            user_type=user_type,
            user_name=user_name,
            phone_number=phone_number,
            address=address,
            **other_fields
        )

        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, user_name, email, password, **other_fields):

        other_fields.setdefault('is_staff', True)
        other_fields.setdefault('is_superuser', True)
        other_fields.setdefault('is_active', True)

        if other_fields.get('is_staff') is not True:
            raise ValueError(
                'Superuser must be assigned to is_staff=True.')

        if other_fields.get('is_superuser') is not True:
            raise ValueError(
                'Superuser must be assigned to is_superuser=True.')

        return self.create_user(user_type=1, user_name=user_name, email=email, password=password, **other_fields)


class AppUser(AbstractBaseUser, PermissionsMixin):
    USER_TYPE_CHOICES = [
        (1, 'Admin'),
        (2, 'Staff'),
        (3, 'Merchant'),
        (4, 'Customer'),
    ]

    user_type = models.PositiveSmallIntegerField(
        choices=USER_TYPE_CHOICES, default=2)

    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255)
    user_name = models.CharField(max_length=255, unique=True)
    email = models.EmailField(max_length=254, unique=True)
    phone_number = models.IntegerField(blank=True, default=0)
    address = models.TextField(blank=True, default='NA')

    created_at = models.DateTimeField(auto_now_add=True)

    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)

    objects = CustomAccountManager()

    USERNAME_FIELD = 'user_name'
    REQUIRED_FIELDS = ['email', 'phone_number', 'address']

    class Meta:
        verbose_name_plural = 'AppUsers'

    def __str__(self):
        return f'{self.user_name} - {self.get_user_type_display()}'

    def __unicode__(self):
        return f'{self.user_name} - {self.get_user_type_display()}'

    def save(self, *args, **kwargs):
        if self.user_type == 4:
            self.is_staff = False
        else:
            self.is_staff = True

        if self.user_type == 1:
            self.is_superuser = True
        else:
            self.is_superuser = False
        super().save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse("admin-user-list")


class Admin(models.Model):
    user = models.OneToOneField(
        AppUser, on_delete=models.CASCADE)

    profile_pic = models.ImageField(
        default='default.png', upload_to='admin_profile_pic')

    def __str__(self):
        return f'{self.user.user_name} - Admin'
