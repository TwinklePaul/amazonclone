from django.db.models.signals import post_save
from django.dispatch import receiver

from .models import (
    AppUser,
    Admin,
)
from Merchant.models import Merchant
from Staff.models import Staff
from Customer.models import Customer


@receiver(post_save, sender=AppUser)
def post_save_create_type(sender, instance, created, **kwargs):
    if created:
        user = instance
        if user.user_type == 1:
            Admin.objects.create(user=user)

        if user.user_type == 2:
            Staff.objects.create(user=user)

        if user.user_type == 3:
            Merchant.objects.create(user=user, company_name="", gst_Details="")

        if user.user_type == 2:
            Customer.objects.create(user=user)
