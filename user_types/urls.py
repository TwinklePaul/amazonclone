from django.urls import path
from . import views
from .views import (
    CreateUser_Admin,
    UserListView_Admin,
    UserDetailView_Admin,
)

urlpatterns = [
    path('login/', views.adminLogin, name='admin-login'),
    path('logout/', views.adminLogout, name='admin-logout'),
    path('', views.adminHome, name='admin-home'),
    path('create-user/', CreateUser_Admin.as_view(), name='admin-create-user'),
    path('all-users/', UserListView_Admin.as_view(), name='admin-user-list'),
    path('user/<pk>/', UserDetailView_Admin.as_view(), name='admin-user-detail'),
]
