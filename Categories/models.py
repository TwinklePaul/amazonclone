from django.db import models
from django.urls import reverse

# Create your models here.


class Category(models.Model):
    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=120)
    url_slug = models.CharField(max_length=50)

    thumbnail = models.ImageField(upload_to='category_images')
    description = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    is_active = models.BooleanField(default=True)

    class Meta:
        verbose_name_plural = 'Categories'

    def __str__(self):
        return f'{self.title} Category'

    def get_absolute_url(self):
        return reverse("category-list")
