from django.urls import path
from .views import (
    CategoryListView_Admin,
    CategoryCreateView_Admin,
    CategoryUpdate_Admin,
    CategoryDelete_Admin,
)

from Subcategory.views import CategoryDetails_Admin

urlpatterns = [
    path('', CategoryListView_Admin.as_view(), name="category-list"),
    path('details/<slug:slug>', CategoryDetails_Admin.as_view(),
         name="category-details"),

    path('create/', CategoryCreateView_Admin.as_view(), name="category-create"),
    path('update/<slug:pk>', CategoryUpdate_Admin.as_view(), name="category-update"),
    path('delete/<int:pk>/', CategoryDelete_Admin.as_view(), name='category-delete'),

]
