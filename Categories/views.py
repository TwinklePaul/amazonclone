from django.shortcuts import render
from django.views.generic import (
    ListView,
    CreateView,
    UpdateView,
    DeleteView
)
from .models import Category
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin

# Create your views here.


class CategoryListView_Admin(LoginRequiredMixin, ListView):
    model = Category
    template_name = 'Categories/admin_templates/list.html'
    context_object_name = 'categories'


class CategoryCreateView_Admin(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    model = Category
    success_message = "Category created successfully!"
    fields = "__all__"
    template_name = 'Categories/admin_templates/edit.html'


class CategoryUpdate_Admin(LoginRequiredMixin, UserPassesTestMixin, SuccessMessageMixin, UpdateView):
    model = Category
    success_message = "Category updated successfully!"
    fields = "__all__"
    template_name = 'Categories/admin_templates/edit.html'

    def test_func(self):
        category = self.get_object()

        return True


class CategoryDelete_Admin(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    model = Category
    template_name = 'Categories/admin_templates/delete.html'
    success_url = '/category'

    def test_func(self):
        category = self.get_object()
        if self.request.user.user_type == 1:
            return True
        return False

