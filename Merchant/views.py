from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic import (
    ListView,
    CreateView,
    UpdateView,
    DetailView
)
from .models import Merchant
from .forms import UserForm, MerchantForm
from user_types.models import AppUser
from django.contrib import messages
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin

# Create your views here.


class MerchantCreateView_Admin(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    model = Merchant
    success_message = "Merchant updated successfully!"
    fields = "__all__"
    template_name = 'Merchant/admin_templates/edit.html'


class MerchantListView_Admin(LoginRequiredMixin, ListView):
    model = Merchant
    template_name = 'Merchant/admin_templates/list.html'
    context_object_name = 'merchants'


class MerchantUpdate_Admin(LoginRequiredMixin, UserPassesTestMixin, SuccessMessageMixin, UpdateView):
    model = Merchant
    success_message = "Merchant updated successfully!"
    fields = [
        "profile_pic",
        "company_name",
        "gst_Details"
    ]
    template_name = 'Merchant/admin_templates/edit.html'

    def test_func(self):
        merchant = self.get_object()

        return True


# class MerchantDetailView_Admin(LoginRequiredMixin, DetailView):
#     model = Merchant
#     template_name = 'Merchant/admin_templates/details.html'
#     context_object_name = 'merchant'

def profile(request, pk):
    if request.method == 'GET':
        merchant = get_object_or_404(Merchant, id=pk)
        m_form = MerchantForm(instance=merchant)
        u_form = UserForm(instance=merchant.user)

    else:
        merchant = get_object_or_404(Merchant, id=pk)
        m_form = MerchantForm(
            request.POST,
            instance=merchant
        )
        u_form = UserForm(request.POST, instance=merchant.user)

        if u_form.is_valid() and m_form.is_valid():
            u_form.save()
            m_form.save()

        messages.success(
            request, f'Your account has been updated!')

        return redirect('merchant-list')

    context = {
        'u_form': u_form,
        'm_form': m_form,
        'merchant': merchant,
    }

    return render(request, 'Merchant/admin_templates/edit.html', context)
