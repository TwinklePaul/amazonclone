from django.urls import path
from .views import (
    MerchantListView_Admin,
    MerchantCreateView_Admin,
    MerchantUpdate_Admin,
)
from .import views

urlpatterns = [

    path('', MerchantListView_Admin.as_view(), name="merchant-list"),
    path('createmerchant/', MerchantCreateView_Admin.as_view(),
         name="merchant-create"),
    path('<pk>/', views.profile, name='merchant-detail'),
    path('update/<pk>', views.profile,
         name="merchant-update"),
]
