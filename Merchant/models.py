from django.db import models
from user_types.models import AppUser, Admin

# Create your models here.


class Merchant(models.Model):
    user = models.OneToOneField(
        AppUser, on_delete=models.CASCADE)

    added_by = models.ManyToManyField(Admin)

    profile_pic = models.ImageField(
        default='default.png', upload_to='merchant_profile_pic')

    company_name = models.CharField(max_length=120)

    gst_Details = models.CharField(max_length=120)

    is_active = models.BooleanField(default=True)

    def __str__(self):
        return f'{self.user.user_name} - \
            {self.company_name} Merchant'
