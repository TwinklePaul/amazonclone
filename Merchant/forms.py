from django import forms
from user_types.models import AppUser
from .models import Merchant


class MerchantForm(forms.ModelForm):
    # company_name = forms.CharField(max_length=255)
    # gst_Details = forms.CharField(max_length=120)
    # profile_pic = forms.ImageField(blank=True)

    class Meta:
        model = Merchant
        fields = [
            'profile_pic',
            'company_name',
            'gst_Details',
        ]


class UserForm(forms.ModelForm):
    # name = forms.CharField(max_length=255)
    # user_name = forms.CharField(max_length=255, unique=True)
    # email = forms.EmailField(max_length=254, unique=True)
    # phone_number = forms.IntegerField(blank=True, default=0)
    # address = forms.TextField(blank=True, default='NA')

    class Meta:
        model = AppUser
        fields = ['name',
                  'user_name',
                  'email',
                  'phone_number',
                  'address', ]
