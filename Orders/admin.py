from django.contrib import admin

from .models import (
    CustomerOrder,
    OrderDeliveryStatus
)
# Register your models here.


admin.site.register(CustomerOrder)
admin.site.register(OrderDeliveryStatus)
