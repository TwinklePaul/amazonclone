from django.db import models
from Products.models import Product


# Create your models here.


class CustomerOrder(models.Model):
    id = models.AutoField(primary_key=True)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    quantity = models.PositiveIntegerField(default=1)
    purchase_price = models.FloatField()
    coupon_code = models.CharField(max_length=20, blank=True, null=True)
    final_amt = models.FloatField()
    product_status = models.CharField(max_length=80)

    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f'{self.product.name} order for amt of Rs. {self.purchase_price}'

    def save(self, *args, **kwargs):
        self.purchase_price = self.product.discount_percent
        return super().save(*args, **kwargs)


class OrderDeliveryStatus(models.Model):

    id = models.AutoField(primary_key=True)
    order = models.ForeignKey(CustomerOrder, on_delete=models.CASCADE)
    status = models.CharField(max_length=120)
    status_message = models.TextField()

    created_at = models.DateTimeField(auto_now_add=True)
    updated_add = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name_plural = 'OrderDeliveryStatuses'

    def __str__(self):
        return f'{self.order.id} - Delivery Status'
